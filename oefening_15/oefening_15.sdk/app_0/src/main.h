#ifndef SRC_MAIN_H_
#define SRC_MAIN_H_

#include <stdio.h>
#include <stdint.h>

/* Xilinx includes */
#include "xparameters.h"
//#include "platform_config.h"
//#include "platform.h"

/* lwIP includes */
#include "lwip/ip_addr.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/init.h"
#include "netif/xadapter.h"
#include "lwip/dhcp.h"
#include "lwipopts.h"

#if __arm__
#include "task.h"
#include "portmacro.h"
#include "xil_printf.h"
int main_thread();
#endif

uint32_t main(void);
static void print_headers();
static void launch_app_threads();
static void print_ip(char *msg, ip_addr_t *ip);
static void print_ip_settings(ip_addr_t *ip, ip_addr_t *mask, ip_addr_t *gw);
uint32_t main_thread(void);

#endif /* SRC_MAIN_H_ */
